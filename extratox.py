from Bio import SeqIO
from collections import defaultdict
from tqdm import tqdm
import subprocess
import multiprocessing as mp
import pandas as pd

try:
    import cPickle as pickle
except ImportError:
    import pickle



dmnd = '/mnt/toolbox/Samuele/gastropods_proteome/testout'
orfile = '/mnt/toolbox/Samuele/pipeline_toxins/convir/convir_01_ORFs/convir_orfs'

WoLFPSort_folder = './software/WoLFPSort/bin/runWolfPsortSummary'

def split_list(l, n_chunks):
    k, m = divmod(len(l), n_chunks)
    return (l[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n_chunks))


def fasta2string(fastadict):
    s = ""
    for i in fastadict.keys():
        s = (s + ">" + i + "\n" + fastadict[i] + "\n")
    return s

def diamond_hits(diamond_res):
    hits_dict = defaultdict(set)
    with open(diamond_res, "r") as diamond_hits:
        print("parsing diamond hits...")
        for line in diamond_hits.readlines():
            split = line.split("\t")[0].split("-")
            cntg = "".join(split[:-1])
            hits_dict[cntg].add(split[-1])
    return hits_dict

def get_diamondable_orfs(filename, diamond_res): #creates a dictionary from orfs that have a hit in diamond and orfs that don't have a diamond hit for the corresponding contig
    hits = diamond_hits(diamond_res)
    orf_dict = defaultdict(str)
    with open(filename, "r") as ORFile:
        print("filtering stage 1 ORFs...")
        for orf in SeqIO.parse(ORFile, "fasta"):
            split = orf.id.split("-")
            cntg = "".join(split[:-1])
            if cntg not in hits.keys():
                orf_dict[orf.id] = str(orf.seq)
            else:
                if split[-1] in hits[cntg]:
                    orf_dict[orf.id] = str(orf.seq)
    return orf_dict

def get_hmmered_orfs(filename, hmmer):
    hmm_dict = defaultdict(set)
    orf_dict = defaultdict(str)
    with open(hmmer, "r") as hmm:
        for line in hmm.readlines():
            if not (line[0] == "#"):
                splt = line.split(" ")[0].split("-")
                cntg = "".join(splt[:-1])
                hmm_dict[cntg].add(splt[-1])
    with open(filename, "r") as ORFile:
        print("filtering stage 2 orfs")
        for orf in SeqIO.parse(ORFile, "fasta"):
            split = orf.id.split("-")
            cntg = "".join(split[:-1])
            if cntg not in hmm_dict.keys():
                orf_dict[orf.id] = str(orf.seq)
            else:
                if split[-1] in hmm_dict[cntg]:
                    orf_dict[orf.id] = str(orf.seq)
    return orf_dict

def deduplicate_seqs(filename):
    print("deduplicating sequences...")
    orf_dict = defaultdict(str)
    seq_set = set()
    with open (filename, "r") as infile:
        for seq in tqdm(SeqIO.parse(infile, "fasta")):
            st_seq = str(seq.seq)
            if st_seq not in seq_set:
                orf_dict[seq.id] = st_seq
                seq_set.add(st_seq)
    return orf_dict

def phobius_extractor(phobfile, fastadict):
    phob_ids = set()
    seq_dict = defaultdict(str)
    print("parsing phobius output...")
    with open(phobfile, "r") as phob:
        phob_ids = set(phob.readlines())
    print("extracting sequences...")
    for i in phob_ids:
        print(fastadict[i])
        seq_dict[i] = fastadict[i]
    return seq_dict

def phobius(filename):
    awk_command = '\'{if ($2 == "0" && $3 == "Y") print ($1)}\''
    command = 'cat {} | sed \'s/\*//g\' | ./software/phobius/phobius.pl -short | awk {}'.format(filename, awk_command)
    #print(command)
    #cat {} | sed \'s/\*//g\' | phobius.pl -short | awk {} | tee {}_phobius.tsv'
    phob_output = subprocess.check_output(command, stderr=subprocess.PIPE, shell=True)
    phob_ids = phob_output.decode('utf-8').split("\n")
    #id = subprocess.Popen(['echo', '-e', '\">{}\n{}\"'.format(id, sequece), "|", 'sed \'s/\*//g\'', '|', 'phobius.pl', '-short', '|', 'awk', awk_command], stdout=subprocess.PIPE).stdout.read()
    phob_set = set()
    for id in phob_ids:
        #print(id)
        phob_set.add(id)
    return phob_set

def parallel_phobius(fastadict, pool, cores):
    jobs = []
    result = defaultdict(str)
    print("running parallel phobius...")
    j = 0
    for ids in split_list(list(fastadict.keys()), cores):
        input_string=""
        for i in ids:
            input_string = input_string + ">" + i + "\n" + fastadict[i] + "\n"
        with open("/tmp/sam_temp"+str(j)+".fasta", "w") as tmpfile:
            tmpfile.write(input_string)
        jobs.append(pool.apply_async(phobius, ("/tmp/sam_temp"+str(j)+".fasta",)))
        j += 1
        '''for id in fastadict.keys():
        jobs.append(pool.apply_async(phobius, (id, fastadict[id], )))'''
    for job in tqdm(jobs):
        ids=job.get()
        for k in ids:
            result[k] = fastadict[k]
    return result

def WoLFPSort(filename):
    awk_command = '\'{if ($2 == "extr") print $1}\''
    cmd = "%s animal < %s | awk %s"%(WoLFPSort_folder, filename, awk_command)
    id = subprocess.check_output(cmd, stderr=subprocess.PIPE, shell=True).decode('utf-8').split("\n")
    wolf_set = set()
    for i in id:
        wolf_set.add(i)
    return wolf_set

def parallel_wolf(fastadict, pool, cores):
    jobs = []
    result = defaultdict(str)
    print("running parallel WoLFPSort...")
    j = 0
    for ids in split_list(list(fastadict.keys()), cores):
        input_string = ""
        for i in ids:
            input_string = input_string + ">" + i + "\n" + fastadict[i] + "\n"
        with open("/tmp/tmp_wolf"+str(j)+".fasta", "w") as tmpfile:
            tmpfile.write(input_string)
        jobs.append(pool.apply_async(WoLFPSort, ("/tmp/tmp_wolf"+str(j)+".fasta", )))
        j += 1
    for job in tqdm(jobs):
        ids=job.get()
        for k in ids:
            result[k] = fastadict[k]
    return result

def remove_contaminants(fastafile, contadb):
    awk_command = 'awk \'{print ($1)}\''
    cmd = "diamond blastp -d %s -q %s | %s"%(contadb, fastafile, awk_command)
    fasta_dic = defaultdict(str)
    print ("parsing fasta...")
    with open(fastafile, "r") as infile:
        for line in SeqIO.parse(infile , "fasta"):
            fasta_dic[line.id] = str(line.seq)
    print("diamond on contaDB...")
    conta_ids = subprocess.check_output(cmd, stderr = subprocess.PIPE, shell=True).decode('utf8').split("\n")
    conta_set = set()
    conta_set.update(conta_ids)
    print("Removing contaminants...")
    for i in conta_set:
        try:
            del(fasta_dic[i])
        except:
            pass
    return fasta_dic

def drop_X(fasta_dic):
    new_dc = defaultdict(str)
    for id in fasta_dic.keys():
        if 'X' not in fasta_dic[id]:
            new_dc[id] = fasta_dic[id]
        else:
            pass
    return new_dc


def run_kallisto(target, out_name, cores, reads_filename):
    print("running Kallisto...")

    print("building index...")
    cmd = ["kallisto", "index", "-i", out_name+"_kallisto_index", target]
    subprocess.run(cmd)

    if(len(reads_filename.split(" ")) >= 2):
        print("running paired-end kallisto...")
        cmd = "kallisto quant -o %s -i %s -t %d %s"%(out_name+".k", out_name+"_kallisto_index", cores, reads_filename)
    else:
        print("running single-end kallisto...")
        cmd = "kallisto quant --single -l 100 -s 1 -o %s -i %s -t %d %s"%(out_name+".k", out_name+"_kallisto_index", cores, reads_filename)


    cmd = cmd.split(" ")
    print (cmd)
    print(subprocess.run(cmd))


def fasta2dict(fastafile):
    dict = defaultdict(str)
    with open(fastafile, "r") as infile:
        for seq in tqdm(SeqIO.parse(fastafile, "fasta")):
            dict[seq.id] = str(seq.seq)
    return dict
    #print(tpmfile.sort_values(by=['tpm'], ascending=False))

def signal_p2(fastafile):
    #signal = subprocess.check_output("signalp -f short -t euk -c 0 /tmp/tmp_signalp0 | awk '{if ($10 == \"Y\") print ($1)}'").decode('utf8').split("\n")
    awk_command = 'awk \'{if ($10 == "Y") print ($1\"\\t\"$8\"\\t\"$3)}\''
    cmd = "signalp -u 0.45 -U 0.45 -f short -t euk -c 0 %s | %s"%(fastafile, awk_command)
    #cmd = cmd.split(" ")
    #print(cmd)
    signal = subprocess.check_output(cmd, shell = True).decode('utf8').split("\n")
    return signal

def signal_p(fastafile):
    #signal = subprocess.check_output("signalp -f short -t euk -c 0 /tmp/tmp_signalp0 | awk '{if ($10 == \"Y\") print ($1)}'").decode('utf8').split("\n")
    #awk_command = 'awk \'{if ($10 != "OTHER") print ($1\"\\t\"$3\"\\t\"$2)}\''
    cmd = "./software/signalp-5.0/bin/signalp -fasta %s -prefix /tmp/signalp_table"%(fastafile)
    #cmd = cmd.split(" ")
    #print(cmd)
    subprocess.run(cmd.split(" "))

def parallel_signalp(fastadict, pool, cores, minscore=0.5):
    jobs = []
    returndic = defaultdict(str)
    scores = []
    surviving = set()
    cores = int (cores)
    i = 0
    if (cores == 0):
        cores = 1
    for ids in split_list(list(fastadict.keys()), cores):
        string = ""
        with open("/tmp/tmp_signalp"+str(i), "w") as outfile:
            for id in ids:
                outfile.write(">" + id + "\n" + str(fastadict[id]) + "\n")
        jobs.append(pool.apply_async(signal_p, ("/tmp/tmp_signalp"+str(i),)))
        i = i+1
    for k in tqdm(jobs):
        a = k.get()
    print("parsing signalp output...")
    results = subprocess.check_output("cat /tmp/signalp_table_summary.signalp5 | grep -e \"SP(Sec/SPI)\" | sed '/^#.*ID/d' | cut -f1,3,5", shell = True).decode('utf8').split("\n")
    
    #print(results)

    for result in results:
        if len(result) > 1:
            if result.split("\t")[0] == "# ID":

                pass
            else:
                values = result.split("\t")#separate score and name from signalp output
                seq = values[0]
                score = float(values[1])
        #populate return tables
                if score >= minscore:
                    scores.append("{}\t{}\t{}".format(seq, values[1], values[2].split(" ")[2][:-1].split("-")[0]))
                    returndic[seq] = fastadict[seq]
                else:
                    pass
    print(returndic)
    subprocess.check_output("rm /tmp/signalp_table_summary.signalp5", shell=True)
    #print(minscore)
    #input()
    try:
        del returndic["# ID"]
    except:
        pass

    return [returndic, scores]


def get_cys_pattern(seq):
    pattern = ""
    status = False
    if seq.count('C') >= 4:
        for char in seq:
            if char == "C":
                pattern = pattern + "C"
                status = True
            else:
                if status:
                    pattern = pattern + "-"
                    status = False
        if(pattern[-1] == "-"):
            pattern = pattern[0:-1]
    return pattern

def diamond_on_db(fastafile, database, outfile, cores, outname):
    print('performing diamond blastp on %s'%database)
    cmd = 'software/diamond/diamond blastp -p %d -d %s -o %s -k 1 -e 1e-6 -q %s -f 6 qseqid sseqid evalue bitscore stitle'%(cores, database, outfile + "_" + outname, fastafile)
    print(cmd)
    subprocess.check_output(cmd, shell = True)
    print(subprocess)
    print("done")


def HMMER(fastafile, cores, database, outfile):
    print("performing HMMER search over %s" %database)
    cmd = "hmmsearch --domtblout %s --cpu %d %s %s"%(outfile + "_hmmer.tsv", cores, database, fastafile)
    print(cmd)
    process = subprocess.check_output(cmd, shell=True)
    print("done")
    print("parsing hits...")
    cmd = "cat %s | sed 's/\\ \\+/\\t/g' | grep -e \"^[a-zA-Z0-9]\" | awk '{print($1\"\\t\"$4)}' > %s"%(outfile+"_hmmer.tsv", outfile+"_hmmer_domains.tsv")
    print(cmd)
    process = subprocess.check_output(cmd, shell = True)
    print("done")

def merge_tables(fastafile, out_name, kallisto = True):
    hmmer = pd.read_csv(out_name + "_hmmer_domains.tsv", sep="\t", names = ["prot_id", "HMMER_domain"])
    print(hmmer.head())
    diamond = pd.read_csv(out_name + "_diamond_uniprot_sprot.tsv", sep = "\t", names = ["prot_id", "BLAST_hit", "evalue", "score", "hit_name"])
    diamond = diamond.drop(columns=[ "score"])
    final = diamond.merge(hmmer.drop_duplicates().groupby('prot_id').agg('; '.join), how = 'outer', on = 'prot_id')
    toxprot = pd.read_csv(out_name + "_diamond_tox.tsv", sep = "\t", names = ["prot_id", "toxprot_hit", "toxprot_evalue", "score", "toxprot_hit_name"])
    toxprot = toxprot.drop(columns = ["score"])
    final = toxprot.merge(final, how = 'outer', on = 'prot_id')
    sigp_score = pd.read_csv(out_name + "_sigP_scores.tsv", sep = "\t", names= ["prot_id", "signalP_score", "cleavage_site"])
    sigp_score.merge(hmmer.drop_duplicates().groupby('prot_id').agg('; '.join), how = 'outer', on = 'prot_id')
    final = sigp_score.merge(final, how = 'outer', on = 'prot_id')
    print(final.head())
    print("loading candidates...")
    candidates = fasta2dict(fastafile)
    seq_df = pd.DataFrame.from_dict(candidates, orient='index', columns = ['AA_sequence'])
    seq_df = seq_df.reset_index()
    seq_df.columns = ['prot_id', 'AA_seq']
    final = seq_df.merge(final, how = 'outer', on = 'prot_id')
    final['signal_peptide'] = final.apply(lambda x: x['AA_seq'][:x['cleavage_site']-1], axis=1)
    final['mature_peptide'] = final.apply(lambda x: x['AA_seq'][x['cleavage_site']-1:], axis=1)
    final['cys_pattern'] = final['mature_peptide'].apply(get_cys_pattern)
    #suca = input()
    #introducing signal-score
    #final['signal-value'] = final['signalP_score'].apply(interpolate_score)


    if kallisto:
#=======================================================================================
            tpmfile = pd.read_csv(out_name+".k/abundance.tsv", header='infer', sep="\t")
            tpm = tpmfile.drop(columns=['length', 'eff_length', 'est_counts'])
            tpm.columns = ['contig', 'tpm']
            tpm.head()
            final['contig'] = final.prot_id.apply(lambda x: "".join(x.split("-")[:-1]))
            final = final.merge(tpm, on= 'contig', how = 'inner')
            suca = input()
            #contiglist.append(contig)
    else:
        final['contig'] = final.prot_id.apply(lambda x: "".join(x.split("-")[:-1]))
        final['tpm'] = "NaN"
    final = final [['prot_id', 'contig', 'AA_seq', 'signalP_score', 'cleavage_site', 'signal_peptide', 'mature_peptide', 'hit_name', 'toxprot_hit_name', 'HMMER_domain', 'cys_pattern', 'tpm' ]]
    print(final)
    #tpmfile = tpmfile[tpmfile['target_id'].isin(contiglist)]
    final.to_csv(out_name+"_mapping_stats.tsv", sep="\t", index = False)

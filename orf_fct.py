#!/usr/bin/python3
#TODO set maximum size
import Bio
import _thread as thread
import sys
from Bio import SeqIO
from collections import defaultdict
import re
from threading import Thread
from tqdm import tqdm
import multiprocessing as mp
from Bio.Seq import Seq

kept_maxlen = 210
jobs = []
rep = 3

frames = [1, 2, 3, -1, -2, -3]

minlen = 25
longest_orf_pattern = re.compile(r'M\w{' + str(minlen-1) +',}\*')
incomplete_orf_pattern = re.compile(r'M\w{' + str(minlen-1) +',}$')

meth=['ATG']
stop=['TAA','TAG','TGA']

maxlen = 120000


#repeat = re.compile(r"(\w{2\})\1\{14,\}")


def parallel_translate(record):
    dc = defaultdict(str)
    contig = set()
    masked = Seq(re.sub(r'(.{2})\1{3,}', "NN"*rep , str(record.seq)))
    for frame in frames:
        try:
            contig.update(auto_translate(frame, masked))
        except:
            print("error in threading")
    j = 0
    ##checking presence of long orfs
    for orf in contig:
        if len(orf) > kept_maxlen:
            contig = set()
            break
    for i in contig:
        #out.write(">{}-{}\n".format(rec.id, j))
        #out.write("{}\n".format(i))
        dc[">{}-{}".format(record.id, j)]= i
        j += 1
    return dc


def auto_translate(frame, seq):
    translated = ""
    orfs = set()
    if frame > 0:
        translated = str(seq[frame-1 : ].translate(table = 1))
        for orf in re.findall(longest_orf_pattern, str(translated)):
            orfs.add(orf)
#        for openorf in re.findall(incomplete_orf_pattern, str(translated)):
#            orfs.add(openorf)
    else:
        seq = seq.reverse_complement()
        frame = frame * (-1)
        translated = str(seq[frame-1 : ].translate(table = 1))
        for orf in re.findall(longest_orf_pattern, str(translated)):
            orfs.add(orf)
#        for openorf in re.findall(incomplete_orf_pattern, str(translated)):
#            orfs.add(openorf)
    return orfs
    #print(d)
    #return d

def stop_to_stop2(minlen, file, out_name, pth, pool):
    dict_t = defaultdict(str)
    with open("{}/{}_01_ORFs/{}_orfs".format(pth,out_name,out_name),"w") as outfile:
        with open(file,"rU") as infile:
            print("Loading contigs...\n")
            for record in tqdm(SeqIO.parse(infile,"fasta")):
                jobs.append(pool.apply_async(parallel_translate, (record, )))
                #pool.apply_async(parallel_translate, (record, dc))
            print("\n\nWaiting for processes to finish...\n")
            for job in tqdm(jobs):
                dict_t.update(job.get())
            #pool.close()
        print("\n\nWriting output...\n")
        for k in tqdm(dict_t.keys()):
            outfile.write("%s\n%s\n" %(k, dict_t[k]))

# seek orfs between two stops only
def stop_to_stop(minlen,file,out_name,dc,meth,pth):

    with open(file,"rU") as infile:
        with open("{}/{}_01_ORFs/{}_orfs".format(pth,out_name,out_name),"w") as outfile:
            for record in SeqIO.parse(infile,"fasta"):

                seq=record.seq
                table=1
                #min_pro_len=minlen #taille minimum des orfs (en AA)
                for strand, nuc in [(+1, seq), (-1, seq.reverse_complement())]:
                    for frame in range(3):
                        i=0
                        b1=str(nuc[frame:])
                        debut=0
                        while i<len(b1):
                            cds="" ### forse non serve
                            if b1[i:i+3] in stop:
                                if debut != 0:
                                    cds=(nuc[frame:])[debut:].translate(table, to_stop=True).split("*")
                                    if len(cds[0])>=int(minlen) and len(cds[0])<=maxlen:
                                        if meth==0:
                                            outfile.write(">{}_strand{}_frame{}_coord{}-{}\n".format(record.id,strand,frame+1,debut,i))
                                            outfile.write("{}\n".format(str(cds[0])))
                                            dc["{}_strand{}_frame{}_coord{}-{}".format(record.id,strand,frame+1,debut,i)]=str(cds[0])
                                        else:
                                            j=0
                                            while j<len(cds[0]):
                                                if (cds[0])[j] in 'M':
                                                    if len((cds[0])[j:])>=int(minlen) and len((cds[0])[j:]) <= ma:
                                                        outfile.write(">{}_strand{}_frame{}_coord{}-{}\n".format(record.id,strand,frame+1,j,i))
                                                        outfile.write("{}\n".format((cds[0])[j:]))
                                                        dc["{}_strand{}_frame{}_coord{}-{}".format(record.id,strand,frame+1,j,i)]=str(cds[0][j:])
                                                    break
                                                j+=1
                                debut=i+3
                            i+=3


# seek all the possible orfs
def all_orfs(minlen,file,out_name,dc,meth,pth):
    with open(file,"rU") as infile:
        print("asd")
        with open("{}/{}_01_ORFs/{}_orfs".format(pth,out_name,out_name),"w") as outfile:
            for record in SeqIO.parse(infile,"fasta"):
                seq=record.seq
                table=1
                #min_pro_len=minlen #taille minimum des orfs (en AA)
                for strand, nuc in [(+1, seq), (-1, seq.reverse_complement())]:
                    for frame in range(3):
                        i=0
                        b1=str(nuc[frame:])
                        debut=0
                        while i<len(b1):
                            cds=""
                            if b1[i:i+3] in stop:
                                pos_stop=i
                                cds=(nuc[frame:])[debut:].translate(table, to_stop=True).split("*")
                                #print(cds[0])
                                if len(cds[0])>=int(minlen) and len(cds[0])<=maxlen:
                                    if meth==0:
                                        outfile.write(">{}_strand{}_frame{}_coord{}-{}\n".format(record.id,strand,frame+1,debut,pos_stop))
                                        outfile.write("{}\n".format(str(cds[0])))
                                        dc["{}_strand{}_frame{}_coord{}-{}".format(record.id,strand,frame+1,debut,i)]=str(cds[0])
                                    else:
                                        j=0
                                        while j<len(cds[0]):
                                            if (cds[0])[j] in 'M':
                                                if len((cds[0])[j:])>=int(minlen):
                                                    outfile.write(">{}_strand{}_frame{}_coord{}-{}\n".format(record.id,strand,frame+1,j,i))
                                                    outfile.write("{}\n".format((cds[0])[j:]))
                                                    dc["{}_strand{}_frame{}_coord{}-{}".format(record.id,strand,frame+1,j,i)]=str(cds[0][j:])
                                                break
                                            j+=1
                                debut=i+3
                            i+=3
                        if cds=="":
                            cds=(nuc[frame:])[debut:].translate(table).split("\n")
                            #print(cds[0])
                            if len(cds[0])>=int(minlen) and len(cds[0])<=maxlen :#added condition for maxlen
                                if meth==0:
                                    outfile.write(">{}_strand{}_frame{}_coord{}-{}\n".format(record.id,strand,frame+1,debut,i))
                                    outfile.write("{}\n".format(str(cds[0])))
                                    dc["{}_strand{}_frame{}_coord{}-{}".format(record.id,strand,frame+1,debut,i)]=str(cds[0])
                                else:
                                    j=0
                                    while j<len(cds[0]):
                                        if (cds[0])[j] in 'M':
                                            if len((cds[0])[j:])>=int(minlen):
                                                outfile.write(">{}_strand{}_frame{}_coord{}-{}\n".format(record.id,strand,frame+1,j,i))
                                                outfile.write("{}\n".format((cds[0])[j:]))
                                                dc["{}_strand{}_frame{}_coord{}-{}".format(record.id,strand,frame+1,j,i)]=str(cds[0][j:])
                                            break
                                        j+=1

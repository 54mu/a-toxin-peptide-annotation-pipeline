#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <map>
#include <iostream>
#include <fstream>
#include <iterator>



std::string revcomp (std::string seq){
    std::string newseq = "";
    std::map<char, char> revc;
    revc.insert(std::make_pair('A', 'T'));
    revc.insert(std::make_pair('T', 'A'));
    revc.insert(std::make_pair('G', 'C'));
    revc.insert(std::make_pair('C', 'G'));
    for (int i = seq.length(); i >= 0; i--){
        newseq = newseq + revc[seq[i]];
    }
    return newseq;
}

int main (int argc, char **argv){
    std::cout << revcomp(argv[1]) << std::endl;
    return 0;
}

#!/usr/bin/python3

import Bio
import sys
from Bio import SeqIO
import re


#those functions are used to retrieve sequences from signalp, phobius and wps outputs


def sign_rc(file,out_name,dc,ds,pth):
        with open (file,'r') as infile1, open("{}/{}_02_STR/{}_signalp".format(pth,out_name,out_name),'w') as outfile1:
                for record in SeqIO.parse(infile1,"fasta"):
                        if record.id in dc.keys():
                                ds[record.id]=dc[record.id]
                                outfile1.write(">{}\n{}\n".format(record.id,dc[record.id]))

def phob_rc(file,out_name,dc,dp,pth):
        with open (file,'r') as infile2, open("{}/{}_02_STR/{}_phobius".format(pth,out_name,out_name),'w') as outfile2:
                for line in infile2:
                        if line.split()[2] == 'Y':
                                dp[line.split()[0]]=(dc[line.split()[0]],line.split()[3].split("/")[1].split("o")[0])
                                outfile2.write(">{}\n{}\n".format(line.split()[0],dc[line.split()[0]]))

def wps_rc(file,out_name,dp,tox,pth):
        with open (file,'r') as infile3, open("{}/{}_02_STR/{}_STR_TOX".format(pth,out_name,out_name),'w') as outfile3:
                for line in infile3:
                        if (re.search(r"^#",line)) == None and (re.search(r"extr",line)) != None:
                                tox[line.split()[0]]=dp[line.split()[0]]
                                outfile3.write(">{}\n{}\n".format(line.split()[0],dp[line.split()[0]][0]))



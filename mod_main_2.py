#!/usr/bin/python3

"""MAIN

Usage:
  mod_main.py -i <file> -o <file> -s <steps> -p <path> [-e <evalue>] [--meth] [--minlen <int>] [--cpu <cpu>] [--stop_to_stop] [--tpm <path_R1,path_R2>] [--kallisto_bootstrap <int>] [--db <path_to_diamond_DB>] [--minscore <value>]

Options:
  -h, --help
  -i <file>     Input file : assembly in fasta format
  -o <file>     Output name
  -s <steps>     Steps : 1 to 4 ; argument can be passed as one int or several (ex : 2 or 234 or 1234 for all steps)
  -p <path>     Path to output dir
  -e <evalue>     Blast threshold evalue [default: 1e-4]
  --meth     Cut ORF before the first methionine. Int this case the minimum length (--minlen) start at the first methionine.
  --minlen <int>     ORF minimum length in amino acids [default: 20]
  --cpu <cpu>     Number of cpu [default: 1]
  --stop_to_stop      Find ORFs included only between two stops. Default is all
  --tpm <path_R1,path_R2>     Path to paired-end reads (R1 & R2) used to assemble
  --kallisto_bootstrap <int>     Number of bootstrap samples [default: 0]
  --minscore <float>   minimum score from signalP. Between 0.5 and 1 [default: 0.5]

"""


from docopt import docopt
import multiprocessing as mp
from tqdm import tqdm
import Bio
import sys
from Bio import SeqIO
import orf_fct
import str_fct
import os
import subprocess
import json
import extratox as ex
import pandas as pd
try:
    import cPickle as pickle
except ImportError:
    import pickle




def write_fasta(dic, filename):
    print("writing file...")
    with open(filename, "w") as outfile:
        for k in tqdm(dic.keys()):
            outfile.write(">%s\n%s\n"%(k, dic[k]))




if __name__ == '__main__':

    arguments = docopt(__doc__, version='0.1.0')
    #########
    cores = int(arguments['--cpu'])

    pool = mp.Pool(cores)
    pth=arguments['-p']
    if not os.path.exists(pth):
            os.mkdir(pth)

    infile=arguments['-i']
    out_name=arguments['-o']
    st=sorted(arguments['-s'])

    #dictionary : dc = orf dict ; ds = mature peptides dict ; dp = phobius dict ; tox = toxins dict ; they are used in str_fct
    manager = mp.Manager()
    dc=manager.dict(lock=True)
    ds={}
    dp={}
    tox={}


    #path to databases (silva, bacteria, toxines, swissprot, pfam) (need a separate param file)
    db_silva='/home/labgen/databases/SILVA'
    db_tox='./databases/Toxprot.dmnd'
    db_swissprot='./databases/uniprot_sprot.dmnd'
    db_conta='/home/labgen/databases/Contaminants/protein/conta'
    db_pfam='./databases/Pfam-A.hmm'

    #db_diamond='/home/labgen/Samuele/cnidaria_database/uniprot_cnidaria+toxin_red.dmnd'
    #db_diamond = arguments['--db']
    db_diamond = './databases/gastropods_prot.dmnd'
    db_diamond_nr = '/home/labgen/databases/BLAST_databases/nr_prot'


    ########## s1=ORFs : uses functions located in orf_fct.py to detect ORFs from assembly in fasta ##########

    if '0' in st:
        print("performing all steps at once")
        print("##################### step 1 started #####################\n")

        pdir1=subprocess.run(['mkdir', '{}/{}_01_ORFs'.format(pth,out_name)], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False)
        #print (pdir1.stdout.decode('utf-8'))


        dc2 = orf_fct.stop_to_stop2(25,infile,out_name,pth, pool)
        '''if arguments['--stop_to_stop']:
                if arguments['--meth']:
                        orf_fct.stop_to_stop(arguments['--minlen'],infile,out_name,dc,1,pth)
                else:
                        orf_fct.stop_to_stop(arguments['--minlen'],infile,out_name,dc,0,pth)

        else:
                if arguments['--meth']:
                        orf_fct.all_orfs(arguments['--minlen'],infile,out_name,dc,1,pth)
                else:
                        orf_fct.all_orfs(arguments['--minlen'],infile,out_name,dc,0,pth)
'''
        with open('{}/{}_01_ORFs/{}_dc'.format(pth,out_name,out_name), 'w') as dc_file:
                json.dump(dc.copy(), dc_file)

        print("\n**** step 1 completed : {} ORFs found ***\n".format(len(dc)))

        first_stage = '{}/{}_01_ORFs/{}_orfs'.format(pth,out_name,out_name)
        pdir2=subprocess.run(['mkdir', '{}/{}_02_BLAST'.format(pth,out_name)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
        with open('{}/{}_01_ORFs/{}_dc'.format(pth,out_name,out_name,'r')) as json_dc_file:
                new_dc = json.load(json_dc_file)

        print("#################### Diamond step started ##################\n")

        diamond = subprocess.run(['diamond', 'blastp', '-p', arguments['--cpu'], '-e', '1e-10','-d', db_diamond, '-q', first_stage,'-o', '{}/{}_02_BLAST/{}_diamond.tsv'.format(pth,out_name,out_name), '-f', '6', 'qseqid', 'sseqid', 'evalue'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
        print("diamond finished, elaborating second stage candidates...")
        second_stage = '{}/{}_02_BLAST/{}_diamond.tsv'.format(pth,out_name,out_name)
        dc = ex.get_diamondable_orfs(first_stage, second_stage)
        second_stage_orfs = '{}/{}_02_BLAST/{}_second_stage.fasta'.format(pth,out_name,out_name)
        write_fasta(dc, second_stage_orfs)
        print("%d sequences survived to diamond" %len(dc.keys()))
        second_stage_orfs_deduplicated = '{}/{}_02_BLAST/{}_second_stage_dedup.fasta'.format(pth,out_name,out_name)
        dc = ex.deduplicate_seqs(second_stage_orfs)
        write_fasta(dc, second_stage_orfs_deduplicated)
        print("%d sequences survived deduplication" %len(dc.keys()))

        print("\n######## step 3 ########\n\n")

        third_stage_folder='{}/{}_03_SIGNALP/{}'.format(pth,out_name, out_name)
        pdir2=subprocess.run(['mkdir', '{}/{}_03_SIGNALP'.format(pth,out_name)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
        print ("starting phobius...")
        #awk_command = '\'{if ($3 == "Y") print ($1)}\''
        #cmd = 'cat {} | sed \'s/\*//g\' | phobius.pl -short | awk {} | tee {}_phobius.tsv'.format(second_stage_orfs_deduplicated,awk_command , third_stage_folder)
        #os.system(cmd)
        ph_dc = ex.parallel_phobius(dc, pool, cores)
        try:
            del ph_dc[""]
        except:
            pass
        write_fasta(ph_dc, third_stage_folder+"_phob.fasta")
        print("%d sequences survived phobius" %len(ph_dc.keys()))

        print ("starting WoLFPSort...")
        wolf_dict = ex.parallel_wolf(ph_dc, pool, cores)
        del wolf_dict[""]
        write_fasta(wolf_dict, third_stage_folder+"_WoLFPSort.fasta")
        print("%d sequences survived WoLFPSort" %len(wolf_dict.keys()))

        print("\nsearching for contaminants...")
        conta_dic = ex.remove_contaminants(third_stage_folder+"_WoLFPSort.fasta", db_conta)
        try:
            del conta_dic[""]
        except:
            pass
        write_fasta(conta_dic, third_stage_folder+"_deconta.fasta")
        print("%d sequences survived decontamination" %len(conta_dic.keys()))

        deXed = ex.drop_X(conta_dic)
        print("%d sequences survived dropping X" %len(deXed.keys()))

        pool.close()


    if '1' in st:
            print("##################### step 1 started #####################\n")

            pdir1=subprocess.run(['mkdir', '{}/{}_01_ORFs'.format(pth,out_name)], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False)
            #print (pdir1.stdout.decode('utf-8'))


            dc2 = orf_fct.stop_to_stop2(25,infile,out_name,pth, pool)
            '''if arguments['--stop_to_stop']:
                    if arguments['--meth']:
                            orf_fct.stop_to_stop(arguments['--minlen'],infile,out_name,dc,1,pth)
                    else:
                            orf_fct.stop_to_stop(arguments['--minlen'],infile,out_name,dc,0,pth)

            else:
                    if arguments['--meth']:
                            orf_fct.all_orfs(arguments['--minlen'],infile,out_name,dc,1,pth)
                    else:
                            orf_fct.all_orfs(arguments['--minlen'],infile,out_name,dc,0,pth)
    '''
        #    with open('{}/{}_01_ORFs/{}_dc'.format(pth,out_name,out_name), 'w') as dc_file:
            #        json.dump(dc2.copy(), dc_file)

            #print("\n**** step 1 completed : {} ORFs found ***\n".format(len(dc2)))
            pool.close()

    ########## s2=str : signalp on the ORF file ; phobius on sequences kept by signalp ; WolfPSort on sequences kept by phobius ##########

    if '2' in st:
            #try:
            first_stage = '{}/{}_01_ORFs/{}_orfs'.format(pth,out_name,out_name)
            pdir2=subprocess.run(['mkdir', '{}/{}_02_BLAST'.format(pth,out_name)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
            #with open('{}/{}_01_ORFs/{}_dc'.format(pth,out_name,out_name,'r')) as json_dc_file:
                    #new_dc = json.load(json_dc_file)

            print("#################### Diamond step started ##################\n")

            diamond = subprocess.run(['diamond', 'blastp', '-p', arguments['--cpu'], '-e', '1e-10','-d', db_diamond, '-q', first_stage,'-o', '{}/{}_02_BLAST/{}_diamond.tsv'.format(pth,out_name,out_name), '-f', '6', 'qseqid', 'sseqid', 'evalue'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
            print(" ".join(['diamond', 'blastp', '-p', arguments['--cpu'], '-e', '1e-10','-d', db_diamond, '-q', first_stage,'-o', '{}/{}_02_BLAST/{}_diamond.tsv'.format(pth,out_name,out_name), '-f', '6', 'qseqid', 'sseqid', 'evalue']))
            #print (arguments['--db'])
            print("diamond finished, elaborating second stage candidates...")
            second_stage = '{}/{}_02_BLAST/{}_diamond.tsv'.format(pth,out_name,out_name)
            dc = ex.get_diamondable_orfs(first_stage, second_stage)
            second_stage_orfs = '{}/{}_02_BLAST/{}_second_stage.fasta'.format(pth,out_name,out_name)
            write_fasta(dc, second_stage_orfs)
            print("%d sequences survived to diamond" %len(dc.keys()))
            second_stage_orfs_deduplicated = '{}/{}_02_BLAST/{}_second_stage_dedup.fasta'.format(pth,out_name,out_name)
            dc = ex.deduplicate_seqs(second_stage_orfs)
            write_fasta(dc, second_stage_orfs_deduplicated)
            print("%d sequences survived deduplication" %len(dc.keys()))





            print("\n######## step 3 ########\n\n")

            third_stage_folder='{}/{}_03_SIGNALP/{}'.format(pth,out_name, out_name)
            pdir2=subprocess.run(['mkdir', '{}/{}_03_SIGNALP'.format(pth,out_name)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
            print("\nsearching for contaminants...")
            conta_dic = ex.remove_contaminants(second_stage_orfs_deduplicated, db_conta)
            try:
                del conta_dic[""]
            except:
                pass

            write_fasta(conta_dic, third_stage_folder+"_deconta.fasta")
            print("%d sequences survived decontamination" %len(conta_dic.keys()))

            deXed = ex.drop_X(conta_dic)
            write_fasta(deXed, third_stage_folder+"_deX.fasta")
            print("%d sequences survived dropping X" %len(deXed.keys()))
            print ("starting phobius...")
            #awk_command = '\'{if ($3 == "Y") print ($1)}\''
            #cmd = 'cat {} | sed \'s/\*//g\' | phobius.pl -short | awk {} | tee {}_phobius.tsv'.format(second_stage_orfs_deduplicated,awk_command , third_stage_folder)
            #os.system(cmd)
            ph_dc = ex.parallel_phobius(deXed, pool, cores)
            try:
                del ph_dc[""]
            except:
                pass
            write_fasta(ph_dc, third_stage_folder+"_phob.fasta")
            print("%d sequences survived phobius" %len(ph_dc.keys()))

            print ("starting WoLFPSort...")
            wolf_dict = ex.parallel_wolf(ph_dc, pool, cores)
            del wolf_dict[""]
            write_fasta(wolf_dict, third_stage_folder+"_WoLFPSort.fasta")
            print("%d sequences survived WoLFPSort" %len(wolf_dict.keys()))


            ###---------signal-P---------

            print("Running parallel signalp...")
            sigp = ex.parallel_signalp(wolf_dict, pool, cores, minscore=float(arguments['--minscore']))
            write_fasta(sigp[0], third_stage_folder+"_sigP.fasta") # pos 0 of parallelsignalp contains the fasta dictionary, pos 1 contains the scores dictionary, to be saved separately for annotation step
            print("%d sequences survived signalP" %len(sigp[0].keys()))
            print("writing scores to file...")
            with open(third_stage_folder+"_sigP_scores.tsv", "w") as score_file:
                score_file.write("\n".join(sigp[1]))
            print("done.")
            pool.close()

    if '3' in st:
        k_stage_folder = '{}/{}_03_SIGNALP/{}'.format(pth,out_name, out_name)
        kallisto = False
        if arguments['--tpm'] != " ":
            kallisto = True
            ex.run_kallisto(infile, k_stage_folder, cores, arguments['--tpm'])
        ex.HMMER(k_stage_folder+"_sigP.fasta", cores, db_pfam, k_stage_folder)
        ex.diamond_on_db(k_stage_folder+"_sigP.fasta", db_swissprot, k_stage_folder, cores, "diamond_uniprot_sprot.tsv")
        ex.diamond_on_db(k_stage_folder+"_sigP.fasta", db_tox, k_stage_folder, cores, "diamond_tox.tsv")
        ex.merge_tables(k_stage_folder+"_sigP.fasta", k_stage_folder, kallisto = kallisto)




    if 'n' in st:
            #try:
            with open('{}/{}_02_STR/{}_tox'.format(pth,out_name,out_name), 'r') as json_tox_file:
                    new_tox = json.load(json_tox_file)
            #except:
             #       print("Can't locate step2 results")

            print("##################### step 3 started #####################\n")

            #creates blast directory
            pdir3=subprocess.run(['mkdir', '{}/{}_03_BLAST'.format(pth,out_name)], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False)
            #print (pdir3.stdout.decode('utf-8'))

            pblast1=subprocess.run(['blastn', '-query', infile, "-db", "{}".format(db_silva), "-out", "{}/{}_03_BLAST/{}_vs_silva".format(pth,out_name,out_name), "-evalue", "{}".format(arguments['-e']), "-outfmt", "6", "-num_threads", arguments['--cpu']], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
            #print (pblast1.stdout.decode('utf-8'))
            if pblast1.stderr.decode('utf-8') == '':
                    print("blast vs silva : done")
            else:
                    print(pblast1.stderr.decode('utf-8'))

            pblast2=subprocess.run(['blastn', '-query', infile, "-db", "{}".format(db_conta), "-out", "{}/{}_03_BLAST/{}_vs_conta".format(pth,out_name,out_name), "-evalue", "{}".format(arguments['-e']), "-outfmt", "6", "-num_threads", arguments['--cpu']], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
            #print (pblast2.stdout.decode('utf-8'))
            if pblast2.stderr.decode('utf-8') == '':
                    print("blast vs conta : done")
            else:
                    print(pblast2.stderr.decode('utf-8'))

            pblast3=subprocess.run(['blastp', '-query', '{}/{}_02_STR/{}_STR_TOX'.format(pth,out_name,out_name), "-db", "{}".format(db_tox), "-out", "{}/{}_03_BLAST/{}_vs_toxDB".format(pth,out_name,out_name), "-evalue", "{}".format(arguments['-e']), "-outfmt", "6", "-num_threads", "{}".format(arguments['--cpu'])], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
            #print (pblast3.stdout.decode('utf-8'))
            if pblast3.stderr.decode('utf-8') == '':
                    print("blast vs toxDB : done")
            else:
                    print(pblast3.stderr.decode('utf-8'))

            pblast4=subprocess.run(['blastp', '-query', '{}/{}_02_STR/{}_STR_TOX'.format(pth,out_name,out_name), "-db", "{}".format(db_swissprot), "-out", "{}/{}_03_BLAST/{}_vs_swissprot".format(pth,out_name,out_name), "-evalue", "{}".format(arguments['-e']), "-outfmt", "6", "-num_threads", arguments['--cpu']], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
            #print (pblast4.stdout.decode('utf-8'))
            if pblast4.stderr.decode('utf-8') == '':
                    print("blast vs swissprot : done")
            else:
                    print(pblast4.stderr.decode('utf-8'))

            phmm=subprocess.run(['hmmsearch', '--cut_ga', '--cpu', arguments['--cpu'], '--tblout', '{}/{}_03_BLAST/{}_tbl.out'.format(pth,out_name,out_name), db_pfam, '{}/{}_02_STR/{}_STR_TOX'.format(pth,out_name,out_name)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
            #print (phmm.stdout.decode('utf-8'))
            if phmm.stderr.decode('utf-8') == '':
                    print("hmmsearch on pfam : done")
            else:
                    print(phmm.stderr.decode('utf-8'))

            pkall=subprocess.run(['kallisto', 'bt', 'index', '-i', '{}/{}_03_BLAST/{}.idx'.format(pth,out_name,out_name), infile], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
            #print (pkall.stdout.decode('utf-8'))
            print("index kallisto : done")

            #pkall2=subprocess.run(['kallisto', 'quant', '-i', '{}/{}_03_BLAST/{}.idx'.format(pth,out_name,out_name), '-o', "{}/{}_03_BLAST/{}".format(pth,out_name,out_name), '-b', arguments['--kallisto_bootstrap'], str(arguments['--tpm'].split(",")[0]), str(arguments['--tpm'].split(",")[1])], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
            #print (pkall2.stdout.decode('utf-8'))
            #print("quantification kallisto : done")




            print("\n**** step 3 completed ***\n")




    ########## s4=sorting putative toxines ##########
    # tri_fct.py is used to sort toxins with tpm and blast results ; and also delete sequences only found in swissprot non toxin database or pfam non toxin db

    if '4' in st:
            #try:
            with open('{}/{}_02_STR/{}_tox'.format(pth,out_name,out_name), 'r') as json_tox_file:
                    new_tox = json.load(json_tox_file)
            #except:
             #       print("Can locate step2 results")

            print("##################### step 4 started #####################\n")

            #creates results directory
            pdir4=subprocess.run(['mkdir', '{}/{}_04_RESULTS'.format(pth,out_name)], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False)
            #print (pdir4.stdout.decode('utf-8'))

            import tri_fct

            tri_fct.tri(new_tox,out_name,pth)

            print("\n**** step 4 completed ***\n")

from PyQt5 import QtCore, QtGui, uic, QtWidgets
from PyQt5.QtWidgets import QFileDialog
import subprocess
import multiprocessing


class Window(QtWidgets.QDialog):
    test = "asdasdasd"
    def __init__(self):
        super(Window,self).__init__()
        uic.loadUi('pipeline.ui', self)

        self.cores.setMaximum(multiprocessing.cpu_count())
        self.cores.setValue(multiprocessing.cpu_count())
        self.cores.setMinimum(1)
        self.fase.setMinimum(1)
        self.fase.setMaximum(3)
        self.process = QtCore.QProcess(self)
        self.process.readyRead.connect(self.dataReady)
        self.RUN.clicked.connect(self.callProgram)

        #disable RUN when process is running
        self.process.started.connect(lambda: self.RUN.setEnabled(False))
        self.process.finished.connect(lambda: self.RUN.setEnabled(True))

    def dataReady(self):
        cursor = self.std_out.textCursor()
        cursor.movePosition(cursor.End)
        cursor.insertText(str(self.process.readAllStandardOutput(), 'utf-8') + str(self.process.readAllStandardError(), 'utf-8'))
        self.std_out.ensureCursorVisible()

    def mybutton_clicked(self):
        options = QFileDialog.Options()
        filename, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "", "All Files (*);;Fasta Files (*.fasta *.fas *.fa)", options=options)
        if filename:
            test = filename
            self.InputEdit.setText(filename)
            self.folder.setText(filename.split("/")[-1].split(".")[0])
            self.filename.setText(filename.split("/")[-1].split(".")[0])

    def database_clicked(self):
        options = QFileDialog.Options()
        filename, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "", "All Files (*);;Diamond Databases (*.dmnd)", options=options)
        if filename:
            test = filename
            self.database.setText(filename)

#================= READS BUTTONS ==================================

    def reads_1_clicked(self):
        options = QFileDialog.Options()
        filename, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "", "All Files (*);;fasta read files (*.fasta *.fas *.fa *.fastq *.fq *.fastq.gz *.fq.gz)", options=options)
        if filename:
            test = filename
            self.reads_1.setText(filename)

    def reads_2_clicked(self):
        options = QFileDialog.Options()
        filename, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "", "All Files (*);;fasta read files (*.fasta *.fas *.fa *.fastq *.fq *.fastq.gz *.fq.gz)", options=options)
        if filename:
            test = filename
            self.reads_2.setText(filename)

#==================================================================


    def RUNcliclked(self):

        fold = self.folder.text()
        name = self.filename.text()
        f = self.fase.value()
        self.std_out.setPlainText("python pipeline.py -i %s -s %d -o %s -n %s --cpu %d " %(inp, f, fold, name, cpu))

    def callProgram(self):
        infile = self.InputEdit.text()
        fold = self.folder.text()
        name = self.filename.text()
        f = self.fase.value()
        cpu = self.cores.value()
        database = self.database.text()
        min_score = self.signal_threshold.value()
        R1 = self.reads_1.text()
        R2 = self.reads_2.text()
        runner = "xterm -hold -e python mod_main_2.py -i {} -s {} -o {} -p {} --cpu {} --db {} --minscore {:2.2f} --tpm \"{} {}\"".format(infile, f, fold, name, cpu, database, min_score, R1, R2)
        print(runner)
        self.process.start(runner)

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = Window()
    window.show()
    sys.exit(app.exec_())
